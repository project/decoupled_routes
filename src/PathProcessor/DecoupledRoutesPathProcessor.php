<?php

namespace Drupal\decoupled_routes\PathProcessor;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for exchanging the base url of decoupled routes.
 */
class DecoupledRoutesPathProcessor implements OutboundPathProcessorInterface {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Creates a DecoupledRoutesPathProcessor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('decoupled_routes.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (isset($options['route_name']) && in_array($options['route_name'], $this->config->get('routes'))) {
      $options['base_url'] = $this->config->get('base_url');
    }
    return $path;
  }

}
